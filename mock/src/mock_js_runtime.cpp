/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "js_runtime.h"
#include "js_runtime_utils.h"
#include "js_extension_context.h"
namespace OHOS::AbilityRuntime {
HandleScope::HandleScope(JsRuntime& jsRuntime) {}
HandleScope::HandleScope(NativeEngine &engine) {}
HandleScope::~HandleScope() {}
NativeValue* HandleScope::Escape(NativeValue* value) { return value; }
std::unique_ptr<Runtime> JsRuntime::Create(const Options& options) {
    class JsRuntimeInner : public JsRuntime {
    public:
        bool RunScript(const std::string &path) override { return false; }
        void StartDebugMode(bool needBreakPoint, int32_t instanceId) override {}
    protected:
        NativeValue *LoadJsModule(const std::string &path) override { return nullptr;}
    };
    return std::make_unique<JsRuntimeInner>();
}

std::unique_ptr<NativeReference> JsRuntime::LoadSystemModuleByEngine(NativeEngine *engine,
                                                                     const std::string &moduleName,
                                                                     NativeValue *const *argv, size_t argc)
{
    return std::unique_ptr<NativeReference>();
}
std::unique_ptr<NativeReference> JsRuntime::LoadModule(const std::string &moduleName, const std::string &modulePath,
                                                       bool esmodule)
{
    return std::unique_ptr<NativeReference>();
}
std::unique_ptr<NativeReference> JsRuntime::LoadSystemModule(const std::string &moduleName, NativeValue *const *argv,
                                                             size_t argc)
{
    return std::unique_ptr<NativeReference>();
}
void JsRuntime::PostTask(const std::function<void()> &task, const std::string &name, int64_t delayTime) {}
void JsRuntime::RemoveTask(const std::string &name) {}
void JsRuntime::DumpHeapSnapshot(bool isPrivate) {}
std::string JsRuntime::BuildJsStackTrace() { return ""; }
void JsRuntime::NotifyApplicationState(bool isBackground) {}
bool JsRuntime::RunSandboxScript(const std::string &path) { return false; }
bool JsRuntime::Initialize(const Runtime::Options &options) { return false; }
void JsRuntime::Deinitialize() {}
NativeValue *JsRuntime::LoadJsBundle(const std::string &path) { return nullptr;}
NativeValue* CreateJsExtensionContext(NativeEngine& engine, const std::shared_ptr<ExtensionContext>& context,
                                      std::shared_ptr<OHOS::AppExecFwk::AbilityInfo> abilityInfo,
                                      DetachCallback detach, AttachCallback attach) { return nullptr; }
}
