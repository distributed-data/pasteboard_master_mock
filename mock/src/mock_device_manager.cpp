/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "device_manager.h"
#include "device_manager_impl.h"
#include "softbus_common.h"
namespace OHOS::DistributedHardware {
DeviceManager &DeviceManager::GetInstance()
{
    return DeviceManagerImpl::GetInstance();
}

DeviceManagerImpl &DeviceManagerImpl::GetInstance()
{
    static DeviceManagerImpl instance;
    return instance;
}
int32_t DeviceManagerImpl::InitDeviceManager(const std::string &pkgName, std::shared_ptr<DmInitCallback> dmInitCallback)
{
    return 0;
}
int32_t DeviceManagerImpl::UnInitDeviceManager(const std::string &pkgName)
{
    return 0;
}

int32_t DeviceManagerImpl::GetTrustedDeviceList(const std::string &pkgName, const std::string &extra,
    std::vector<DmDeviceInfo> &deviceList)
{
    DmDeviceInfo remote;
    (void)strcpy_s(remote.networkId, DM_MAX_DEVICE_ID_LEN, "64a0a92a428005db27c40bad46bf145fede38ec37effe0347cd990fcb031f320");
    (void)strcpy_s(remote.deviceId, DM_MAX_DEVICE_ID_LEN, "64a0a92a428005db27c40bad46bf145fede38ec37effe0347cd990fcb031f320");
    (void)strcpy_s(remote.deviceName, DM_MAX_DEVICE_ID_LEN, "demo2 phone");
    remote.deviceTypeId = SMART_PHONE;
    deviceList.push_back(remote);
    return 0;
}

int32_t DeviceManagerImpl::GetLocalDeviceInfo(const std::string &pkgName, DmDeviceInfo &deviceInfo)
{
    // (void)strcpy_s(info->networkId, NETWORK_ID_BUF_LEN, "1252645812135842132135452A3B4C5D6E7F");
    (void)strcpy_s(deviceInfo.networkId, DM_MAX_DEVICE_ID_LEN, "54a0a92a428005db27c40bad46bf145fede38ec37effe0347cd990fcb031f320");
    (void)strcpy_s(deviceInfo.deviceId, DM_MAX_DEVICE_ID_LEN, "54a0a92a428005db27c40bad46bf145fede38ec37effe0347cd990fcb031f320");
    (void)strcpy_s(deviceInfo.deviceName, DM_MAX_DEVICE_ID_LEN, "demo phone");
    deviceInfo.deviceTypeId = SMART_PHONE;
    return 0;
}
int32_t DeviceManagerImpl::RegisterDevStateCallback(const std::string &pkgName, const std::string &extra,
    std::shared_ptr<DeviceStateCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::UnRegisterDevStateCallback(const std::string &pkgName)
{
    return 0;
}
int32_t DeviceManagerImpl::StartDeviceDiscovery(const std::string &pkgName, const DmSubscribeInfo &subscribeInfo,
    const std::string &extra, std::shared_ptr<DiscoveryCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::StopDeviceDiscovery(const std::string &pkgName, uint16_t subscribeId)
{
    return 0;
}
int32_t DeviceManagerImpl::AuthenticateDevice(const std::string &pkgName, int32_t authType,
    const DmDeviceInfo &deviceInfo, const std::string &extra, std::shared_ptr<AuthenticateCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::UnAuthenticateDevice(const std::string &pkgName, const DmDeviceInfo &deviceInfo)
{
    return 0;
}
int32_t DeviceManagerImpl::VerifyAuthentication(const std::string &pkgName, const std::string &authPara,
    std::shared_ptr<VerifyAuthCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::RegisterDeviceManagerFaCallback(const std::string &packageName,
    std::shared_ptr<DeviceManagerFaCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::UnRegisterDeviceManagerFaCallback(const std::string &pkgName)
{
    return 0;
}
int32_t DeviceManagerImpl::GetFaParam(const std::string &pkgName, DmAuthParam &faParam)
{
    return 0;
}
int32_t DeviceManagerImpl::SetUserOperation(const std::string &pkgName, int32_t action)
{
    return 0;
}
int32_t DeviceManagerImpl::GetUdidByNetworkId(const std::string &pkgName, const std::string &netWorkId,
    std::string &udid)
{
    if (netWorkId == std::string("no_exist_device_id") || netWorkId.find("invalid_device") != std::string::npos) {
        return -1;
    }
    udid = netWorkId;
    return 0;
}
int32_t DeviceManagerImpl::GetUuidByNetworkId(const std::string &pkgName, const std::string &netWorkId,
    std::string &uuid)
{
    if (netWorkId == std::string("no_exist_device_id") || netWorkId.find("invalid_device") != std::string::npos) {
        return -1;
    }
    uuid = netWorkId;
    return 0;
}
int32_t DeviceManagerImpl::RegisterDevStateCallback(const std::string &pkgName, const std::string &extra)
{
    return 0;
}
int32_t DeviceManagerImpl::UnRegisterDevStateCallback(const std::string &pkgName, const std::string &extra)
{
    return 0;
}
int32_t DeviceManagerImpl::RequestCredential(const std::string &pkgName, const std::string &reqJsonStr,
    std::string &returnJsonStr)
{
    return 0;
}
int32_t DeviceManagerImpl::ImportCredential(const std::string &pkgName, const std::string &credentialInfo)
{
    return 0;
}
int32_t DeviceManagerImpl::DeleteCredential(const std::string &pkgName, const std::string &deleteInfo)
{
    return 0;
}
int32_t DeviceManagerImpl::RegisterCredentialCallback(const std::string &pkgName,
    std::shared_ptr<CredentialCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::UnRegisterCredentialCallback(const std::string &pkgName)
{
    return 0;
}
int32_t DeviceManagerImpl::PublishDeviceDiscovery(const std::string &pkgName, const DmPublishInfo &publishInfo,
    std::shared_ptr<PublishCallback> callback)
{
    return 0;
}
int32_t DeviceManagerImpl::UnPublishDeviceDiscovery(const std::string &pkgName, int32_t publishId)
{
    return 0;
}
int32_t DeviceManagerImpl::NotifyEvent(const std::string &pkgName, const int32_t eventId, const std::string &event)
{
    return 0;
}
}