/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "hilog/log.h"
#include "hisysevent.h"

namespace OHOS {
namespace HiviewDFX {
int HiLog::Debug(const HiLogLabel &label, const char *fmt, ...) { return 0; }

int HiLog::Info(const HiLogLabel &label, const char *fmt, ...) { return 0; }

int HiLog::Warn(const HiLogLabel &label, const char *fmt, ...) { return 0; }

int HiLog::Error(const HiLogLabel &label, const char *fmt, ...) { return 0; }

int HiLog::Fatal(const HiLogLabel &label, const char *fmt, ...) { return 0; }

void HiSysEvent::AppendHexData(EventBase &eventBase, const std::string &key, uint64_t value) {}

int HiSysEvent::CheckArraySize(unsigned long size) { return 0; }

int HiSysEvent::CheckKey(const std::string &key) { return 0; }

int HiSysEvent::CheckValue(const std::string &value) { return 0; }

void HiSysEvent::ExplainRetCode(EventBase &eventBase) {}

void HiSysEvent::InnerWrite(EventBase &eventBase) {}

bool HiSysEvent::IsError(EventBase &eventBase) { return false; }

bool HiSysEvent::IsErrorAndUpdate(int retCode, EventBase &eventBase) { return false; }

bool HiSysEvent::IsWarnAndUpdate(int retCode, EventBase &eventBase) { return false; }

void HiSysEvent::SendSysEvent(EventBase &eventBase) { ; }

void HiSysEvent::WritebaseInfo(EventBase &eventBase) { ; }

unsigned int HiSysEvent::GetArrayMax() { return 0; }
void HiSysEvent::AppendValue(HiSysEvent::EventBase &eventBase, const std::string &item)
{

}
void HiSysEvent::AppendValue(HiSysEvent::EventBase &eventBase, const char item)
{

}
void HiSysEvent::AppendValue(HiSysEvent::EventBase &eventBase, const unsigned char item)
{

}
bool HiSysEvent::UpdateAndCheckKeyNumIsOver(HiSysEvent::EventBase &eventBase)
{
    return false;
}
}
}