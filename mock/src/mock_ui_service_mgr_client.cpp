/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ui_service_mgr_client.h"
namespace OHOS::Ace {
std::shared_ptr<UIServiceMgrClient> UIServiceMgrClient::instance_ = std::make_shared<UIServiceMgrClient>();
std::shared_ptr<UIServiceMgrClient> UIServiceMgrClient::GetInstance()
{
    return instance_;
}
UIServiceMgrClient::UIServiceMgrClient()
{
}
UIServiceMgrClient::~UIServiceMgrClient()
{
}
ErrCode UIServiceMgrClient::RegisterCallBack(const AAFwk::Want &want, const sptr<IUIService> &uiService)
{
    return 0;
}
ErrCode UIServiceMgrClient::UnregisterCallBack(const AAFwk::Want &want)
{
    return 0;
}
ErrCode UIServiceMgrClient::Push(const AAFwk::Want &want, const std::string &name, const std::string &jsonPath,
    const std::string &data, const std::string &extraData)
{
    return 0;
}
ErrCode UIServiceMgrClient::Request(const AAFwk::Want &want, const std::string &name, const std::string &data)
{
    return 0;
}
ErrCode UIServiceMgrClient::ReturnRequest(const AAFwk::Want &want, const std::string &source, const std::string &data,
    const std::string &extraData)
{
    return 0;
}
ErrCode UIServiceMgrClient::ShowDialog(const std::string &name, const std::string &params,
    OHOS::Rosen::WindowType windowType, int x, int y, int width, int height, DialogCallback callback, int *id)
{
    return 0;
}
ErrCode UIServiceMgrClient::CancelDialog(int32_t id)
{
    return 0;
}
ErrCode UIServiceMgrClient::UpdateDialog(int32_t id, const std::string &data)
{
    return 0;
}
ErrCode UIServiceMgrClient::ShowAppPickerDialog(const AAFwk::Want &want,
    const std::vector<AppExecFwk::AbilityInfo> &abilityInfos, int32_t userId)
{
    return 0;
}
}