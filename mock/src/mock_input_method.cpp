/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "input_method_controller.h"
#include "input_method_agent_proxy.h"
#include "input_attribute.h"
#include "input_method_system_ability_proxy.h"
namespace OHOS::MiscServices {
sptr<InputMethodController> InputMethodController::GetInstance()
{
    return new InputMethodController();
}

void InputMethodController::Attach(sptr<OnTextChangedListener> &listener)
{
}

void InputMethodController::Attach(sptr<OnTextChangedListener> &listener, bool isShowKeyboard)
{
}

std::u16string InputMethodController::GetTextBeforeCursor(int32_t number)
{
    return std::u16string();
}

std::u16string InputMethodController::GetTextAfterCursor(int32_t number)
{
    return std::u16string();
}

void InputMethodController::ShowTextInput()
{
}

void InputMethodController::HideTextInput()
{
}

void InputMethodController::Close()
{
}

void InputMethodController::OnRemoteSaDied(const wptr<IRemoteObject> &object)
{
}

void InputMethodController::OnCursorUpdate(CursorInfo cursorInfo)
{
}

void InputMethodController::OnSelectionChange(std::u16string text, int start, int end)
{
}

void InputMethodController::OnConfigurationChange(Configuration info)
{
}

bool InputMethodController::dispatchKeyEvent(std::shared_ptr<MMI::KeyEvent> keyEvent)
{
    return false;
}

int32_t InputMethodController::DisplayOptionalInputMethod()
{
    return 0;
}

std::vector<Property> InputMethodController::ListInputMethodCommon(InputMethodStatus status)
{
    return std::vector<Property>();
}

std::vector<Property> InputMethodController::ListInputMethod()
{
    return std::vector<Property>();
}

std::vector<Property> InputMethodController::ListInputMethod(bool enable)
{
    return std::vector<Property>();
}

int32_t InputMethodController::GetEnterKeyType()
{
    return 0;
}

int32_t InputMethodController::GetInputPattern()
{
    return 0;
}

std::shared_ptr<Property> InputMethodController::GetCurrentInputMethod()
{
    return std::shared_ptr<Property>();
}

int32_t InputMethodController::HideCurrentInput()
{
    return 0;
}

int32_t InputMethodController::ShowCurrentInput()
{
    return 0;
}

void InputMethodController::SetCallingWindow(uint32_t windowId)
{
}

int32_t InputMethodController::SwitchInputMethod(const Property &target)
{
    return 0;
}

InputMethodController::InputMethodController()
{
}

InputMethodController::~InputMethodController()
{
}

bool InputMethodController::Initialize()
{
    return false;
}

sptr<InputMethodSystemAbilityProxy> InputMethodController::GetImsaProxy()
{
    return sptr<InputMethodSystemAbilityProxy>();
}

void InputMethodController::PrepareInput(int32_t displayId, sptr<InputClientStub> &client,
    sptr<InputDataChannelStub> &channel, InputAttribute &attribute)
{
}
void InputMethodController::StartInput(sptr<InputClientStub> &client, bool isShowKeyboard)
{
}
void InputMethodController::StopInput(sptr<InputClientStub> &client)
{
}
void InputMethodController::ReleaseInput(sptr<InputClientStub> &client)
{
}
void InputMethodController::SetInputMethodAgent(sptr<IRemoteObject> &object)
{
}
std::shared_ptr<InputMethodAgentProxy> InputMethodController::GetInputMethodAgent()
{
    return std::shared_ptr<InputMethodAgentProxy>();
}
void InputMethodController::WorkThread()
{
}
InputMethodAgentProxy::InputMethodAgentProxy(const sptr<IRemoteObject> &object): IRemoteProxy(object) {};
bool InputMethodAgentProxy::DispatchKeyEvent(MessageParcel &data)
{
    return false;
}
void InputMethodAgentProxy::OnCursorUpdate(int32_t positionX, int32_t positionY, int32_t height)
{
}
void InputMethodAgentProxy::OnSelectionChange(std::u16string text, int32_t oldBegin, int32_t oldEnd, int32_t newBegin,
    int32_t newEnd)
{
}
void InputMethodAgentProxy::SetCallingWindow(uint32_t windowId)
{
}
InputAttribute::InputAttribute()
{
}
InputAttribute::InputAttribute(const InputAttribute &attribute)
{
}
InputAttribute &InputAttribute::operator=(const InputAttribute &attribute)
{
    return *this;
}
InputAttribute::~InputAttribute()
{
}
bool InputAttribute::Marshalling(Parcel &parcel) const
{
    return false;
}
InputAttribute *InputAttribute::Unmarshalling(Parcel &parcel)
{
    return nullptr;
}
void InputAttribute::SetInputPattern(int32_t pattern)
{
}
bool InputAttribute::GetSecurityFlag()
{
    return false;
}
InputMethodSystemAbilityProxy::InputMethodSystemAbilityProxy(const sptr<IRemoteObject> &object) : IRemoteProxy(object)
{
}
void InputMethodSystemAbilityProxy::prepareInput(MessageParcel &data)
{
}
void InputMethodSystemAbilityProxy::releaseInput(MessageParcel &data)
{
}
void InputMethodSystemAbilityProxy::startInput(MessageParcel &data)
{
}
void InputMethodSystemAbilityProxy::stopInput(MessageParcel &data)
{
}
void InputMethodSystemAbilityProxy::SetCoreAndAgent(MessageParcel &data)
{
}
int32_t InputMethodSystemAbilityProxy::HideCurrentInput(MessageParcel &data)
{
    return 0;
}
int32_t InputMethodSystemAbilityProxy::ShowCurrentInput(MessageParcel &data)
{
    return 0;
}
int32_t InputMethodSystemAbilityProxy::Prepare(int32_t displayId, sptr<InputClientStub> &client,
    sptr<InputDataChannelStub> &channel, InputAttribute &attribute)
{
    return 0;
}
int32_t InputMethodSystemAbilityProxy::Release(sptr<InputClientStub> &client)
{
    return 0;
}
int32_t InputMethodSystemAbilityProxy::Start(sptr<InputClientStub> &client)
{
    return 0;
}
int32_t InputMethodSystemAbilityProxy::Stop(sptr<InputClientStub> &client)
{
    return 0;
}
int32_t InputMethodSystemAbilityProxy::displayOptionalInputMethod(MessageParcel &data)
{
    return 0;
}
int32_t InputMethodSystemAbilityProxy::getDisplayMode(int32_t &retMode)
{
    return 0;
}
int32_t InputMethodSystemAbilityProxy::getKeyboardWindowHeight(int32_t &retHeight)
{
    return 0;
}
std::shared_ptr<InputMethodProperty> InputMethodSystemAbilityProxy::GetCurrentInputMethod()
{
    return std::shared_ptr<InputMethodProperty>();
}
int32_t InputMethodSystemAbilityProxy::getCurrentKeyboardType(KeyboardType *retType)
{
    return 0;
}
std::vector<InputMethodProperty> InputMethodSystemAbilityProxy::ListInputMethod(InputMethodStatus stauts)
{
    return std::vector<InputMethodProperty>();
}
int32_t InputMethodSystemAbilityProxy::listKeyboardType(const std::u16string &imeId, std::vector<KeyboardType *> *types)
{
    return 0;
}
int32_t InputMethodSystemAbilityProxy::SwitchInputMethod(const InputMethodProperty &target)
{
    return 0;
}
}
