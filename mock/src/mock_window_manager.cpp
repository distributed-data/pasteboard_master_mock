/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "window_manager.h"
namespace OHOS::Rosen {
class WindowManager::Impl {
public:
    int32_t code_;
};
WindowManager &WindowManager::GetInstance()
{
    static WindowManager instance;
    return instance;
}
WindowManager::WindowManager()
{
}
void WindowManager::RegisterFocusChangedListener(const sptr<IFocusChangedListener> &listener)
{
}
void WindowManager::UnregisterFocusChangedListener(const sptr<IFocusChangedListener> &listener)
{
}
void WindowManager::RegisterSystemBarChangedListener(const sptr<ISystemBarChangedListener> &listener)
{
}
}