/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <hks_param.h>
#include <hks_api.h>
#include "securec.h"
int32_t HksInitParamSet(struct HksParamSet **paramSet) { return 0; }
int32_t HksAddParams(struct HksParamSet *paramSet, const struct HksParam *params, uint32_t paramCnt)
{
    return 0;
}
int32_t HksBuildParamSet(struct HksParamSet **paramSet)
{
    return 0;
}
void HksFreeParamSet(struct HksParamSet **paramSet)
{

}
int32_t HksGetParamSet(const struct HksParamSet *fromParamSet, uint32_t fromParamSetSize, struct HksParamSet **paramSet)
{
    return 0;
}
int32_t HksGetParam(const struct HksParamSet *paramSet, uint32_t tag, struct HksParam **param)
{
    return 0;
}
int32_t HksFreshParamSet(struct HksParamSet *paramSet, bool isCopy)
{
    return 0;
}
int32_t HksCheckParamSetTag(const struct HksParamSet *paramSet)
{
    return 0;
}
int32_t HksCheckParamSet(const struct HksParamSet *paramSet, uint32_t size)
{
    return 0;
}
int32_t HksCheckParamMatch(const struct HksParam *baseParam, const struct HksParam *param)
{
    return 0;
}
int32_t HksGenerateKey(const struct HksBlob *keyAlias,
    const struct HksParamSet *paramSetIn, struct HksParamSet *paramSetOut) { return 0; }
int32_t HksGetSdkVersion(struct HksBlob *sdkVersion)
{
    return 0;
}
int32_t HksInitialize(void)
{
    return 0;
}
int32_t HksRefreshKeyInfo(void)
{
    return 0;
}
int32_t HksImportKey(const struct HksBlob *keyAlias, const struct HksParamSet *paramSet, const struct HksBlob *key)
{
    return 0;
}
int32_t HksExportPublicKey(const struct HksBlob *keyAlias, const struct HksParamSet *paramSet, struct HksBlob *key)
{
    return 0;
}
int32_t HksDeleteKey(const struct HksBlob *keyAlias, const struct HksParamSet *paramSet)
{
    return 0;
}
int32_t HksGetKeyParamSet(const struct HksBlob *keyAlias, const struct HksParamSet *paramSetIn,
    struct HksParamSet *paramSetOut)
{
    return 0;
}
int32_t HksKeyExist(const struct HksBlob *keyAlias, const struct HksParamSet *paramSet)
{
    return 0;
}
int32_t HksGenerateRandom(const struct HksParamSet *paramSet, struct HksBlob *random)
{
    return 0;
}
int32_t HksSign(const struct HksBlob *key, const struct HksParamSet *paramSet, const struct HksBlob *srcData,
    struct HksBlob *signature)
{
    return 0;
}
int32_t HksVerify(const struct HksBlob *key, const struct HksParamSet *paramSet, const struct HksBlob *srcData,
    const struct HksBlob *signature)
{
    return 0;
}
int32_t HksEncrypt(const struct HksBlob *key, const struct HksParamSet *paramSet, const struct HksBlob *plainText,
    struct HksBlob *cipherText)
{
    (void)memcpy_s(cipherText->data, cipherText->size, plainText->data, plainText->size);
    cipherText->size = plainText->size;
    return 0;
}
int32_t HksDecrypt(const struct HksBlob *key, const struct HksParamSet *paramSet, const struct HksBlob *cipherText,
    struct HksBlob *plainText)
{
    (void)memcpy_s(plainText->data, plainText->size, cipherText->data, cipherText->size);
    plainText->size = cipherText->size;
    return 0;
}
int32_t HksAgreeKey(const struct HksParamSet *paramSet, const struct HksBlob *privateKey,
    const struct HksBlob *peerPublicKey, struct HksBlob *agreedKey)
{
    return 0;
}
int32_t HksDeriveKey(const struct HksParamSet *paramSet, const struct HksBlob *mainKey, struct HksBlob *derivedKey)
{
    return 0;
}
int32_t HksMac(const struct HksBlob *key, const struct HksParamSet *paramSet, const struct HksBlob *srcData,
    struct HksBlob *mac)
{
    return 0;
}
int32_t HksHash(const struct HksParamSet *paramSet, const struct HksBlob *srcData, struct HksBlob *hash)
{
    return 0;
}
int32_t HksGetKeyInfoList(const struct HksParamSet *paramSet, struct HksKeyInfo *keyInfoList, uint32_t *listCount)
{
    return 0;
}
int32_t HksAttestKey(const struct HksBlob *keyAlias, const struct HksParamSet *paramSet, struct HksCertChain *certChain)
{
    return 0;
}
int32_t HksGetCertificateChain(const struct HksBlob *keyAlias, const struct HksParamSet *paramSet,
    struct HksCertChain *certChain)
{
    return 0;
}
int32_t HksWrapKey(const struct HksBlob *keyAlias, const struct HksBlob *targetKeyAlias,
    const struct HksParamSet *paramSet, struct HksBlob *wrappedData)
{
    return 0;
}
int32_t HksUnwrapKey(const struct HksBlob *keyAlias, const struct HksBlob *targetKeyAlias,
    const struct HksBlob *wrappedData, const struct HksParamSet *paramSet)
{
    return 0;
}
int32_t HksBnExpMod(struct HksBlob *x, const struct HksBlob *a, const struct HksBlob *e, const struct HksBlob *n)
{
    return 0;
}
