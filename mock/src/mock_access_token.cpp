/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "accesstoken_kit.h"
#include "nativetoken_kit.h"
#include "token_setproc.h"
static constexpr const char *BundleNames[60] = {
    "invalid",
    "com.huawei.ohos.toteweather",
    "com.ohos.kvdatamanager.test",
    "ohos.test.demo"
};
static constexpr const char *AppId[60] = {
    "invalid",
    "com.huawei.toteweather_adfasdflaskdfasdf",
    "",
    "ohos.test.demo_ABSEED"
};
static constexpr const char *NativeName[60] = {
    "invalid",
    "foundation",
    "distributed_test",
};
namespace OHOS::Security::AccessToken {
ATokenTypeEnum AccessTokenKit::GetTokenTypeFlag(AccessTokenID tokenID)
{
    AccessTokenIDInner *inner = (AccessTokenIDInner *)&tokenID;
    return ATokenTypeEnum(inner->type);
}

int AccessTokenKit::VerifyAccessToken(AccessTokenID tokenID, const std::string &permissionName)
{
    return PERMISSION_GRANTED;
}

int AccessTokenKit::GetHapTokenInfo(AccessTokenID tokenID, HapTokenInfo &hapTokenInfoRes)
{
    AccessTokenIDInner *inner = (AccessTokenIDInner *)&tokenID;
    hapTokenInfoRes.ver = 0;
    hapTokenInfoRes.userID = 100;
    hapTokenInfoRes.instIndex = inner->tokenUniqueID / 60;
    hapTokenInfoRes.deviceID = "54a0a92a428005db27c40bad46bf145fede38ec37effe0347cd990fcb031f320";
    hapTokenInfoRes.tokenID = tokenID;
    if (inner->type == TOKEN_NATIVE) {
        hapTokenInfoRes.apl = APL_SYSTEM_CORE;
        hapTokenInfoRes.bundleName = NativeName[inner->tokenUniqueID % 60];
        hapTokenInfoRes.appID = hapTokenInfoRes.bundleName;
    } else {
        hapTokenInfoRes.apl = APL_NORMAL;
        hapTokenInfoRes.bundleName = BundleNames[inner->tokenUniqueID % 60];
        hapTokenInfoRes.appID = AppId[inner->tokenUniqueID % 60];
    }
    return 0;
}

int AccessTokenKit::GetNativeTokenInfo(AccessTokenID tokenID, NativeTokenInfo &nativeTokenInfoRes)
{
    AccessTokenIDInner *inner = (AccessTokenIDInner *)&tokenID;
    nativeTokenInfoRes.ver = 0;
    nativeTokenInfoRes.tokenID = tokenID;
    if (inner->type == TOKEN_NATIVE) {
        nativeTokenInfoRes.apl = APL_SYSTEM_CORE;
        nativeTokenInfoRes.processName = NativeName[inner->tokenUniqueID % 60];
    } else {
        nativeTokenInfoRes.apl = APL_NORMAL;
        nativeTokenInfoRes.processName = BundleNames[inner->tokenUniqueID % 60];
    }
    return 0;
}

AccessTokenID AccessTokenKit::GetHapTokenID(int userID, const std::string& bundleName, int instIndex)
{
    uint32_t tokenId = 0;
    auto *inner = (AccessTokenIDInner *)&tokenId;
    inner->type = 3;
    for (int i = 0; i < 60; ++i) {
        if (BundleNames[i] == nullptr) {
            break;
        }
        if (bundleName == BundleNames[i]) {
            inner->type = TOKEN_HAP;
            inner->tokenUniqueID = i + 60 * instIndex;
            break;
        }
    }
    return tokenId;
}
AccessTokenIDEx AccessTokenKit::AllocHapToken(const HapInfoParams &info, const HapPolicyParams &policy)
{
    AccessTokenIDEx result;
    AccessTokenIDInner *inner = (AccessTokenIDInner *)&result.tokenIdExStruct.tokenID;
    inner->tokenUniqueID = 0;
    inner->type = TOKEN_HAP;
    return result;
}
int AccessTokenKit::DeleteToken(AccessTokenID tokenID)
{
    return 0;
}

int AccessTokenKit::GrantPermission(AccessTokenID tokenID, const std::string &permissionName, int flag)
{
    return 0;
}
}

uint64_t GetAccessTokenId(NativeTokenInfoParams *params)
{
    uint32_t tokenId = 0;
    auto *inner = (OHOS::Security::AccessToken::AccessTokenIDInner *)&tokenId;
    inner->type = 3;
    for (int i = 0; i < 60; ++i) {
        if (NativeName[i] == nullptr) {
            break;
        }
        if (strcmp(NativeName[i], params->processName) == 0) {
            inner->type = OHOS::Security::AccessToken::TOKEN_NATIVE;
            inner->tokenUniqueID = i;
            break;
        }
    }
    return tokenId;
}

int SetSelfTokenID(uint64_t tokenID)
{
    return 0;
}