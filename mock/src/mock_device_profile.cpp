/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "distributed_device_profile_client.h"
#include "service_characteristic_profile.h"
#include "subscribe_info.h"
#include "sync_options.h"
namespace OHOS::DeviceProfile {
void ServiceCharacteristicProfile::SetServiceId(const std::string &serviceId)
{
    serviceId_ = serviceId;
};
void ServiceCharacteristicProfile::SetServiceType(const std::string &serviceType)
{
    serviceType_ = serviceType;
}
void ServiceCharacteristicProfile::SetServiceProfileJson(const std::string &profileJson)
{
    serviceProfileJson_ = profileJson;
}
void ServiceCharacteristicProfile::SetCharacteristicProfileJson(const std::string &profileJson)
{
    characteristicProfileJson_ = profileJson;
}
const std::string &ServiceCharacteristicProfile::GetServiceId() const
{
    return serviceId_;
}
const std::string &ServiceCharacteristicProfile::GetServiceType() const
{
    return serviceType_;
}
const std::string &ServiceCharacteristicProfile::GetServiceProfileJson() const
{
    return serviceProfileJson_;
}
const std::string &ServiceCharacteristicProfile::GetCharacteristicProfileJson() const
{
    return characteristicProfileJson_;
}
bool ServiceCharacteristicProfile::Marshalling(Parcel &parcel) const
{
    return true;
}
bool ServiceCharacteristicProfile::Unmarshalling(Parcel &parcel)
{
    return true;
}
int32_t DistributedDeviceProfileClient::PutDeviceProfile(const ServiceCharacteristicProfile &profile)
{
    return 0;
}
DistributedDeviceProfileClient &DistributedDeviceProfileClient::GetInstance()
{
    static DistributedDeviceProfileClient instance;
    return instance;
}
int32_t DistributedDeviceProfileClient::GetDeviceProfile(const std::string &udid, const std::string &serviceId,
    ServiceCharacteristicProfile &profile)
{
    profile.SetCharacteristicProfileJson("{\"distributed_pasteboard_enabled\": \"true\"}");
    return 0;
}
int32_t DistributedDeviceProfileClient::DeleteDeviceProfile(const std::string &serviceId)
{
    return 0;
}
int32_t DistributedDeviceProfileClient::SubscribeProfileEvent(const SubscribeInfo &subscribeInfo,
    const std::shared_ptr<IProfileEventCallback> &eventCb)
{
    return 0;
}
int32_t DistributedDeviceProfileClient::UnsubscribeProfileEvent(ProfileEvent profileEvent,
    const std::shared_ptr<IProfileEventCallback> &eventCb)
{
    return 0;
}
int32_t DistributedDeviceProfileClient::SubscribeProfileEvents(const std::list<SubscribeInfo> &subscribeInfos,
    const std::shared_ptr<IProfileEventCallback> &eventCb, std::list<ProfileEvent> &failedEvents)
{
    return 0;
}
int32_t DistributedDeviceProfileClient::UnsubscribeProfileEvents(const std::list<ProfileEvent> &profileEvents,
    const std::shared_ptr<IProfileEventCallback> &eventCb, std::list<ProfileEvent> &failedEvents)
{
    return 0;
}
int32_t DistributedDeviceProfileClient::SyncDeviceProfile(const SyncOptions &syncOptions,
    const std::shared_ptr<IProfileEventCallback> &syncCb)
{
    return 0;
}
sptr<IDistributedDeviceProfile> DistributedDeviceProfileClient::GetDeviceProfileService()
{
    return sptr<IDistributedDeviceProfile>();
}
bool DistributedDeviceProfileClient::CheckProfileInvalidity(const ServiceCharacteristicProfile &profile)
{
    return false;
}
void DistributedDeviceProfileClient::OnServiceDied(const sptr<IRemoteObject> &remote)
{
}
void DistributedDeviceProfileClient::MergeSubscribeInfoLocked(std::list<SubscribeInfo> &subscribeInfos,
    const std::list<SubscribeInfo> &newSubscribeInfos)
{
}

SyncMode SyncOptions::GetSyncMode() const
{
    return SyncMode::PUSH_PULL;
}
const std::list<std::string> &SyncOptions::GetDeviceList() const
{
    return syncDevIds_;
}
void SyncOptions::AddDevice(const std::string &deviceId)
{
    syncDevIds_.push_back(deviceId);
}
void SyncOptions::SetSyncMode(SyncMode mode)
{
    syncMode_ = mode;
}
bool SyncOptions::Marshalling(Parcel &parcel) const
{
    return true;
}
bool SyncOptions::Unmarshalling(Parcel &parcel)
{
    return true;
}
bool SubscribeInfo::Marshalling(Parcel &parcel) const
{
    return false;
}
bool SubscribeInfo::Unmarshalling(Parcel &parcel)
{
    return false;
}
bool SubscribeInfo::operator!=(const SubscribeInfo &rhs) const
{
    return false;
};
} // namespace OHOS::DeviceProfile