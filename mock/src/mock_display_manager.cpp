/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <display.h>

#include "display_manager.h"
namespace OHOS::Rosen {
DisplayManager &DisplayManager::GetInstance()
{
    static DisplayManager instance;
    return instance;
}
std::vector<sptr<Display>> DisplayManager::GetAllDisplays()
{
    return std::vector<sptr<Display>>();
}
DisplayId DisplayManager::GetDefaultDisplayId()
{
    return 0;
}
sptr<Display> DisplayManager::GetDefaultDisplay()
{
    return sptr<Display>();
}
sptr<Display> DisplayManager::GetDisplayById(DisplayId displayId)
{
    return sptr<Display>();
}
sptr<Display> DisplayManager::GetDisplayByScreen(ScreenId screenId)
{
    return sptr<Display>();
}
std::vector<DisplayId> DisplayManager::GetAllDisplayIds()
{
    return std::vector<DisplayId>();
}
bool DisplayManager::RegisterDisplayListener(sptr<IDisplayListener> listener)
{
    return false;
}
bool DisplayManager::UnregisterDisplayListener(sptr<IDisplayListener> listener)
{
    return false;
}
std::shared_ptr<Media::PixelMap> DisplayManager::GetScreenshot(DisplayId displayId)
{
    return std::shared_ptr<Media::PixelMap>();
}
std::shared_ptr<Media::PixelMap> DisplayManager::GetScreenshot(DisplayId displayId, const Media::Rect &rect,
    const Media::Size &size, int rotation)
{
    return std::shared_ptr<Media::PixelMap>();
}
bool DisplayManager::RegisterDisplayPowerEventListener(sptr<IDisplayPowerEventListener> listener)
{
    return false;
}
bool DisplayManager::UnregisterDisplayPowerEventListener(sptr<IDisplayPowerEventListener> listener)
{
    return false;
}
bool DisplayManager::WakeUpBegin(PowerStateChangeReason reason)
{
    return false;
}
bool DisplayManager::WakeUpEnd()
{
    return false;
}
bool DisplayManager::SuspendBegin(PowerStateChangeReason reason)
{
    return false;
}
bool DisplayManager::SuspendEnd()
{
    return false;
}
bool DisplayManager::SetDisplayState(DisplayState state, DisplayStateCallback callback)
{
    return false;
}
DisplayState DisplayManager::GetDisplayState(DisplayId displayId)
{
    return DisplayState::UNKNOWN;
}
bool DisplayManager::SetScreenBrightness(uint64_t screenId, uint32_t level)
{
    return false;
}
uint32_t DisplayManager::GetScreenBrightness(uint64_t screenId) const
{
    return 0;
}
void DisplayManager::NotifyDisplayEvent(DisplayEvent event)
{
}
bool DisplayManager::Freeze(std::vector<DisplayId> displayIds)
{
    return false;
}
bool DisplayManager::Unfreeze(std::vector<DisplayId> displayIds)
{
    return false;
}
DisplayManager::DisplayManager()
{
}
DisplayManager::~DisplayManager()
{
}
Display::~Display() {}
DisplayId Display::GetId() const
{
    return 0;
}
int32_t Display::GetWidth() const
{
    return 1280;
}
int32_t Display::GetHeight() const
{
    return 1920;
}
uint32_t Display::GetRefreshRate() const
{
    return 0;
}
ScreenId Display::GetScreenId() const
{
    return 0;
}
float Display::GetVirtualPixelRatio() const
{
    return 0;
}
int Display::GetDpi() const
{
    return 0;
}
Rotation Display::GetRotation() const
{
    return Rotation::ROTATION_90;
}
Orientation Display::GetOrientation() const
{
    return Orientation::BEGIN;
}
sptr<DisplayInfo> Display::GetDisplayInfo() const
{
    return sptr<DisplayInfo>();
}
Display::Display(const std::string &name, sptr<DisplayInfo> info)
{
}
void Display::UpdateDisplayInfo(sptr<DisplayInfo>) const
{
}
void Display::UpdateDisplayInfo() const
{
}
}
