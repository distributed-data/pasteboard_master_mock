@echo off
@echo on
cls
:start
@echo ------------------------------------------------
@echo "Create the pasteboard store dir and config.json"
@echo %date%-%time%
@echo "Input the cygwin install dir like D:\cygwin64"
@set /p cypwin_dir=Please input(full name):
mkdir %cypwin_dir%\system\etc\pasteboard\conf
mkdir %cypwin_dir%\data\service\el1\public\database\pasteboard_service
copy .\pasteboard_proprietary\etc\conf\pasteboard.json %cypwin_dir%\system\etc\pasteboard\conf
@echo "SUCCESS"
pause