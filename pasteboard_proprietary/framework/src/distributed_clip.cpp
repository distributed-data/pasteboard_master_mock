/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "distributed_clip.h"

#include <chrono>
#include <thread>
#include "distributed_kv_data_manager.h"
#include "sync_callback.h"
#include "utils.h"
#include "dfx/pasteboard_trace.h"
namespace OHOS::MiscServices {
using namespace OHOS::DistributedKv;
using namespace std::chrono;
DistributedClip::DistributedClip()
{
    blockObject_ = std::make_shared<BlockObject<GlobalEvent>>(BLOCK_INTERVAL, GlobalEvent());
    Initialize();
    DMAdapter::GetInstance().Register(this);
}

DistributedClip::~DistributedClip()
{
    DMAdapter::GetInstance().Unregister(this);
    std::unique_lock<decltype(rwMutex_)> write(rwMutex_);
    DeInit();
}

int32_t DistributedClip::SetPasteData(const GlobalEvent &event, const std::vector<uint8_t> &data)
{
    GlobalEvent evt = event;
    evt.frameNum = (data.size() + Utils::MAX_VALUE_SIZE - 1) / Utils::MAX_VALUE_SIZE;
    if (data.size() <= MAX_EVT_ADDITION) {
        evt.frameNum = 0;
        evt.addition = data;
    }
    std::shared_lock<decltype(rwMutex_)> read(rwMutex_);
    if (store_ == nullptr && !Initialize()) {
        // todo error
        return {};
    }

    PasteboardTrace trace("distributed_clip");
    store_->RemoveDeviceData("");
    if (evt.frameNum > 0) {
        auto entries = Utils::Marshal(evt, data);
        store_->PutBatch(entries);
    }
    auto rawEvent = Utils::Marshal(evt);
    auto evtKey = Utils::GetEventKey(evt);
    store_->Put(evtKey, rawEvent);
    DoSyncEvent(evtKey, event.expiration);
    blockObject_->SetValue(event);
    return 0;
}

int32_t DistributedClip::GetPasteData(const GlobalEvent &event, std::vector<uint8_t> &data)
{
    DataQuery query;
    query.KeyPrefix(Utils::GetDataPrefix(event));
    int count = 0;
    std::shared_lock<decltype(rwMutex_)> read(rwMutex_);
    if (store_ == nullptr && !Initialize()) {
        // todo error
        return {};
    }

    store_->GetCount(query, count);
    if (count < event.frameNum) {
        auto callback = std::make_shared<SyncCallback>(DATA_SYNC_INTERVAL);
        auto status = store_->Sync({ event.deviceId }, PULL, query, callback);
        if (status != SUCCESS || callback->GetSyncStatus(event.deviceId) != SUCCESS) {
            return -1;
        }
    }

    std::vector<Entry> entries;
    store_->GetEntries(query, entries);
    data = Utils::Unmarshal(event, entries);
    return 0;
}

std::vector<ClipPlugin::GlobalEvent> DistributedClip::GetTopEvents(uint32_t topN, int32_t user)
{
    DataQuery query;
    query.KeyPrefix(Utils::EVT_PREFIX);
    query.OrderByWriteTime(false);
    query.Limit(topN, 0);
    std::vector<Entry> entries;
    std::shared_lock<decltype(rwMutex_)> read(rwMutex_);
    if (store_ == nullptr && !Initialize()) {
        // todo error
        return {};
    }

    store_->GetEntries(query, entries);
    std::vector<ClipPlugin::GlobalEvent> events;
    for (Entry &entry : entries) {
        auto event = Utils::Unmarshal(entry.value);
        events.push_back(std::move(event));
    }
    (void)user;
    return events;
}

void DistributedClip::Clear(int32_t user)
{
    {
        std::lock_guard<decltype(mutex_)> lockGuard(mutex_);
        lastSyncKey_ = "";
        expiration_ = 0;
    }
    (void)user;
    std::unique_lock<decltype(rwMutex_)> write(rwMutex_);
    if (store_ == nullptr && !Initialize()) {
        return;
    }
    store_->RemoveDeviceData("");
    GlobalEvent event;
    event.deviceId = DMAdapter::GetInstance().GetLocalDevice();
    event.user = user;
    RemoveDirtyData(event, store_);
}

void DistributedClip::Online(const std::string &device)
{
    DoRetrySync({ device });
}

void DistributedClip::Offline(const std::string &device)
{
}

void DistributedClip::DoSyncEvent(const std::string &evtKey, uint64_t expiration)
{
    {
        std::shared_lock<decltype(rwMutex_)> read(rwMutex_);
        if (store_ == nullptr) {
            return;
        }

        DataQuery query;
        query.KeyPrefix(evtKey);
        store_->Sync({}, PUSH_PULL, query, std::make_shared<SyncCallback>(EVT_SYNC_INTERVAL));
    }
    {
        std::lock_guard<decltype(mutex_)> lockGuard(mutex_);
        lastSyncKey_ = evtKey;
        expiration_ = expiration;
    }
}

void DistributedClip::DoRetrySync(const std::vector<std::string> &devices)
{
    uint64_t curTime = duration_cast<milliseconds>((system_clock::now()).time_since_epoch()).count();
    std::string evtKey;
    {
        std::lock_guard<decltype(mutex_)> lockGuard(mutex_);
        if (curTime >= expiration_) {
            return;
        }
        evtKey = lastSyncKey_;
    }

    {
        std::shared_lock<decltype(rwMutex_)> read(rwMutex_);
        if (store_ == nullptr) {
            return;
        }
        DataQuery query;
        query.KeyPrefix(evtKey);
        store_->Sync(devices, PUSH, query);
    }
}

void DistributedClip::DeInit(bool destroy)
{
    (void)destroy;
    blockObject_->SetValue(GlobalEvent());
    store_ = nullptr;
    DistributedKvDataManager manager;
    manager.CloseKvStore({ APP_ID }, { STORE_ID });
}

bool DistributedClip::Initialize()
{
    int32_t retry = 0;
    DistributedKvDataManager manager;
    Options options;
    options.backup = false;
    options.autoSync = false;
    options.rebuild = true;
    options.baseDir = BASE_DIR;
    options.securityLevel = S3;
    options.area = EL1;
    options.kvStoreType = SINGLE_VERSION;
    while (store_ == nullptr && retry < 2) {
        manager.GetSingleKvStore(options, { APP_ID }, { STORE_ID }, store_);
        if (store_ == nullptr) {
            // todo sleep 100ms
        }
        retry++;
    }
    if (store_ != nullptr) {
        DataQuery query;
        query.KeyPrefix(Utils::EVT_PREFIX);
        store_->Sync({}, PUSH_PULL, query);
        std::thread remover([blocker = blockObject_, store = store_](){
            GlobalEvent event;
            for (;;) {
                event = blocker->GetValue();
                if (event.deviceId.empty()) {
                    break;
                }
                DistributedClip::RemoveDirtyData(event, store);
            }
        });
        remover.detach();
    }
    return store_ != nullptr;
}

bool DistributedClip::RemoveDirtyData(const GlobalEvent &event, std::shared_ptr<Store> store)
{
    std::shared_ptr<KvStoreResultSet> resultSet;
    auto prefix = Utils::GetAllDataPrefix(event);
    auto status = store->GetResultSet(prefix, resultSet);
    if (status != SUCCESS) {
        // todo
        return false;
    }

    Key exceptPrefix = Utils::GetDataPrefix(event);
    std::vector<Key> deleteKeys;
    while (resultSet->MoveToNext()) {
        Entry entry;
        resultSet->GetEntry(entry);
        if (!entry.key.StartsWith(exceptPrefix)) {
            deleteKeys.push_back(std::move(entry.key));
        }
    }
    if (event.seqId == 0) {
        deleteKeys.push_back(Utils::GetEventKey(event));
    }
    status = store->DeleteBatch(deleteKeys);
    return status == SUCCESS;
}
} // namespace OHOS::MiscServices
