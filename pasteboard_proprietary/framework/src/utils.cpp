/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "utils.h"

#include "securec.h"
namespace OHOS::MiscServices {
uint32_t Utils::index_ = 0;
constexpr size_t Utils::HEAD_SIZE;
std::string Utils::GetEventKey(const Event &event)
{
    std::string evtKey = EVT_PREFIX;
    evtKey.append(SPLIT)
        .append(event.deviceId)
        .append(SPLIT)
        .append(std::to_string(event.user))
        .append(SPLIT)
        .append(std::to_string(GetCurrentIndex()));
    return evtKey;
}

std::string Utils::GetDataKey(const Event &event, uint32_t frame)
{
    std::string evtKey = DATA_PREFIX;
    evtKey.append(SPLIT)
        .append(event.deviceId)
        .append(SPLIT)
        .append(std::to_string(event.seqId))
        .append(SPLIT)
        .append(std::to_string(frame));
    return evtKey;
}

std::string Utils::GetDataPrefix(const Event &event)
{
    std::string evtKey = DATA_PREFIX;
    evtKey.append(SPLIT).append(event.deviceId).append(SPLIT).append(std::to_string(event.seqId));
    return evtKey;
}

std::string Utils::GetAllDataPrefix(const Utils::Event &event)
{
    std::string evtKey = std::string(DATA_PREFIX).append(SPLIT).append(event.deviceId);
    return evtKey;
}

std::vector<uint8_t> Utils::Marshal(const Event &event)
{
    size_t size = HEAD_SIZE + event.deviceId.size() + event.account.size() + event.addition.size();
    size_t offset = 0;
    std::vector<uint8_t> rawData(size);
    Write(rawData.data(), offset, size, event.version);
    Write(rawData.data(), offset, size, event.frameNum);
    Write(rawData.data(), offset, size, event.user);
    Write(rawData.data(), offset, size, event.seqId);
    Write(rawData.data(), offset, size, event.expiration);
    Write(rawData.data(), offset, size, event.status);
    uint16_t length = event.deviceId.size();
    Write(rawData.data(), offset, size, length);
    length = event.account.size();
    Write(rawData.data(), offset, size, length);
    length = event.addition.size();
    Write(rawData.data(), offset, size, length);
    Write(rawData.data(), offset, size, event.deviceId.data(), event.deviceId.size());
    Write(rawData.data(), offset, size, event.account.data(), event.account.size());
    Write(rawData.data(), offset, size, event.addition.data(), event.addition.size());
    return rawData;
}

Utils::Event Utils::Unmarshal(const std::vector<uint8_t> &rawData)
{
    Event event;
    size_t size = rawData.size();
    size_t offset = 0;
    Read(rawData.data(), offset, size, event.version);
    Read(rawData.data(), offset, size, event.frameNum);
    Read(rawData.data(), offset, size, event.user);
    Read(rawData.data(), offset, size, event.seqId);
    Read(rawData.data(), offset, size, event.expiration);
    Read(rawData.data(), offset, size, event.status);
    uint16_t length = 0;
    Read(rawData.data(), offset, size, length);
    event.deviceId.resize(length);
    Read(rawData.data(), offset, size, length);
    event.account.resize(length);
    Read(rawData.data(), offset, size, length);
    event.addition.resize(length);
    Read(rawData.data(), offset, size, event.deviceId.data(), event.deviceId.size());
    Read(rawData.data(), offset, size, event.account.data(), event.account.size());
    Read(rawData.data(), offset, size, event.addition.data(), event.addition.size());

    // just finish the version 0
    return event;
}

std::vector<Utils::Entry> Utils::Marshal(const Event &event, const std::vector<uint8_t> &data)
{
    std::vector<Entry> entries(event.frameNum);
    size_t remain = data.size();
    size_t offset = 0;
    for (uint8_t i = 0; i < event.frameNum; ++i) {
        Entry &entry = entries[i];
        entry.key = GetDataKey(event, i);
        size_t pkgLen = std::min(remain, MAX_VALUE_SIZE);
        entry.value = std::vector<uint8_t>(data.begin() + offset, data.begin() + offset + pkgLen);
        offset += pkgLen;
        remain -= pkgLen;
    }
    return entries;
}

std::vector<uint8_t> Utils::Unmarshal(const Event &event, const std::vector<Entry> &entries)
{
    if (entries.size() != event.frameNum) {
        return {};
    }

    size_t size = 0;
    for (auto &entry : entries) {
        size += entry.value.Size();
    }
    std::vector<uint8_t> rawData(size);
    for (auto &entry : entries) {
        size_t offset = GetFrameNo(entry.key) * MAX_VALUE_SIZE;
        rawData.insert(rawData.begin() + offset, entry.value.Data().begin(), entry.value.Data().end());
        size -= entry.value.Size();
    }
    return rawData;
}

uint32_t Utils::GetCurrentIndex()
{
    return (++index_) % MAX_RECORD;
}

template<class T>
bool Utils::Write(uint8_t *canvas, size_t &offset, size_t &size, T value)
{
    value = HToLe(value);
    auto err = memcpy_s(canvas + offset, size, reinterpret_cast<uint8_t *>(&value), sizeof(T));
    size -= sizeof(T);
    offset += sizeof(T);
    return err == EOK;
}

bool Utils::Write(uint8_t *canvas, size_t &offset, size_t &size, const void *value, size_t length)
{
    if (length == 0) {
        return true;
    }

    auto err = memcpy_s(canvas + offset, size, value, length);
    size -= length;
    offset += length;
    return err == EOK;
}

template<class T>
bool Utils::Read(const uint8_t *canvas, size_t &offset, size_t &size, T &value)
{
    auto err = memcpy_s(reinterpret_cast<uint8_t *>(&value), sizeof(T), canvas + offset, std::min(size, sizeof(T)));
    size -= sizeof(T);
    offset += sizeof(T);
    value = LeToH(value);
    return err == EOK;
}

bool Utils::Read(const uint8_t *canvas, size_t &offset, size_t &size, void *value, size_t length)
{
    if (length == 0) {
        return true;
    }

    auto copyLen = std::min(size, length);
    auto err = memcpy_s(value, length, canvas + offset, copyLen);
    size -= copyLen;
    offset += copyLen;
    return err == EOK;
}

uint32_t Utils::GetFrameNo(const std::vector<uint8_t> &key)
{
    return (key[key.size() - 1] - '0');
}
}