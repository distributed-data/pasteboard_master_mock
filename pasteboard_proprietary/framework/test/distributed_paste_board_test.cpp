/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cstdint>
#include <gtest/gtest.h>
#include <vector>
#include <chrono>

#include "clip/clip_plugin.h"
#include "pasteboard_client.h"
#include "pasteboard_observer_callback.h"
#include "pixel_map.h"
#include "uri.h"

using namespace testing::ext;
using namespace OHOS;
using namespace OHOS::AAFwk;
using namespace OHOS::MiscServices;
using namespace OHOS::Media;
using namespace std::chrono;

class DistributedPasteBoardTest : public testing::Test {
public:
    static void SetUpTestCase(void);
    static void TearDownTestCase(void);
    void SetUp();
    void TearDown();
};

void DistributedPasteBoardTest::SetUpTestCase(void)
{
}

void DistributedPasteBoardTest::TearDownTestCase(void)
{
}

void DistributedPasteBoardTest::SetUp(void)
{
}

void DistributedPasteBoardTest::TearDown(void)
{
    PasteboardClient::GetInstance()->Clear();
}

/**
* @tc.name: GetDistributedNormalData
* @tc.desc: Get the normal distributed data.
* @tc.type: FUNC
* @tc.require:
* @tc.author: Sven Wang
*/
HWTEST_F(DistributedPasteBoardTest, GetDistributedNormalData, TestSize.Level0)
{
    ClipPlugin *plugin = ClipPlugin::CreatePlugin("distributed_clip");
    uint64_t expiration = duration_cast<milliseconds>((system_clock::now() + minutes(2)).time_since_epoch()).count();
    ClipPlugin::GlobalEvent event;
    event.user = 0;
    event.seqId = 10000;
    event.expiration = expiration;
    event.deviceId = "valid_deviceId_0x0000";
    event.account = "";
    event.status = ClipPlugin::EVT_NORMAL;
    PasteData inputData;
    inputData.AddHtmlRecord("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    std::vector<uint8_t> value;
    inputData.Encode(value);
    plugin->SetPasteData(event, value);

    auto has = PasteboardClient::GetInstance()->HasPasteData();
    ASSERT_TRUE(has);
    PasteData pasteData;
    auto ok = PasteboardClient::GetInstance()->GetPasteData(pasteData);
    PASTEBOARD_HILOGI(PASTEBOARD_MODULE_SERVICE, "get.");
    ASSERT_TRUE(ok == true);
    auto html = pasteData.GetPrimaryHtml();
    ASSERT_TRUE(*html == "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
}

/**
* @tc.name: GetDistributedInvalidData
* @tc.desc: Get the invalid distributed data.
* @tc.type: FUNC
* @tc.require:
* @tc.author: Sven Wang
*/
HWTEST_F(DistributedPasteBoardTest, GetDistributedInvalidData, TestSize.Level0)
{
    ClipPlugin *plugin = ClipPlugin::CreatePlugin("distributed_clip");
    uint64_t expiration = duration_cast<milliseconds>((system_clock::now() + minutes(2)).time_since_epoch()).count();
    ClipPlugin::GlobalEvent event;
    event.user = 0;
    event.seqId = 10000;
    event.expiration = expiration;
    event.deviceId = "valid_deviceId_0x0001";
    event.account = "";
    event.status = ClipPlugin::EVT_INVALID;
    PasteData inputData;
    inputData.AddHtmlRecord("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    std::vector<uint8_t> value;
    inputData.Encode(value);
    plugin->SetPasteData(event, value);

    auto has = PasteboardClient::GetInstance()->HasPasteData();
    ASSERT_FALSE(has);
    PasteData pasteData;
    auto success = PasteboardClient::GetInstance()->GetPasteData(pasteData);
    PASTEBOARD_HILOGI(PASTEBOARD_MODULE_SERVICE, "get.");
    ASSERT_FALSE(success);
}

/**
* @tc.name: GetLocalDataWhenDistributedDataInvalid
* @tc.desc: Get the local data, when the distributed data is invalid.
* @tc.type: FUNC
* @tc.require:
* @tc.author: Sven Wang
*/
HWTEST_F(DistributedPasteBoardTest, GetLocalDataWhenDistributedDataInvalid, TestSize.Level0)
{
    auto record = PasteboardClient::GetInstance()->CreatePlainTextRecord("paste record1");
    ASSERT_TRUE(record != nullptr);
    std::string plainText = "plain text";
    auto data = PasteboardClient::GetInstance()->CreatePlainTextData(plainText);
    ASSERT_TRUE(data != nullptr);
    PasteboardClient::GetInstance()->SetPasteData(*data);
    ClipPlugin *plugin = ClipPlugin::CreatePlugin("distributed_clip");
    uint64_t expiration = duration_cast<milliseconds>((system_clock::now() + minutes(2)).time_since_epoch()).count();
    ClipPlugin::GlobalEvent event;
    event.user = 0;
    event.seqId = 10000;
    event.expiration = expiration;
    event.deviceId = "valid_deviceId_0x0002";
    event.account = "";
    event.status = ClipPlugin::EVT_INVALID;
    PasteData inputData;
    inputData.AddHtmlRecord("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    std::vector<uint8_t> value;
    inputData.Encode(value);
    plugin->SetPasteData(event, value);

    auto has = PasteboardClient::GetInstance()->HasPasteData();
    ASSERT_TRUE(has);
    PasteData pasteData;
    auto success = PasteboardClient::GetInstance()->GetPasteData(pasteData);
    PASTEBOARD_HILOGI(PASTEBOARD_MODULE_SERVICE, "get.");
    ASSERT_TRUE(success);
    auto primaryText = pasteData.GetPrimaryText();
    ASSERT_TRUE(primaryText != nullptr);
    ASSERT_TRUE(*primaryText == plainText);
}

