/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef DISTRIBUTED_PASTEBOARD_FRAMEWORK_SYNC_RESULT_H
#define DISTRIBUTED_PASTEBOARD_FRAMEWORK_SYNC_RESULT_H
#include "common/block_object.h"
#include "kvstore_sync_callback.h"
namespace OHOS::MiscServices {
class SyncCallback : public DistributedKv::KvStoreSyncCallback {
public:
    using Status = DistributedKv::Status;
    SyncCallback(uint32_t interval) : result_(interval) {};
    virtual ~SyncCallback() = default;
    void SyncCompleted(const std::map<std::string, Status> &results) override;
    Status GetSyncStatus(const std::string &device);

private:
    BlockObject<std::map<std::string, Status>> result_;
};
} // namespace OHOS::MiscServices
#endif // DISTRIBUTED_PASTEBOARD_FRAMEWORK_SYNC_RESULT_H
