/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DISTRIBUTED_PASTEBOARD_FRAMEWORK_UTILS_H
#define DISTRIBUTED_PASTEBOARD_FRAMEWORK_UTILS_H
#include <endian.h>
#include "clip/clip_plugin.h"
#include "types.h"
namespace OHOS::MiscServices {
class Utils {
public:
    static constexpr size_t MAX_VALUE_SIZE = 4 * 1024 * 1024;
    static constexpr const char *EVT_PREFIX = "EVENT";
    static constexpr const char *DATA_PREFIX = "DATA";
    using Entry = DistributedKv::Entry;
    using Event = ClipPlugin::GlobalEvent;
    static std::string GetEventKey(const Event &event);
    static std::string GetDataKey(const Event &event, uint32_t frame);
    static std::string GetDataPrefix(const Event &event);
    static std::string GetAllDataPrefix(const Event &event);
    static std::vector<uint8_t> Marshal(const Event &event);
    static Event Unmarshal(const std::vector<uint8_t> &rawData);
    static std::vector<Entry> Marshal(const Event &event, const std::vector<uint8_t> &data);
    static std::vector<uint8_t> Unmarshal(const Event &event, const std::vector<Entry> &entries);

    static uint32_t GetCurrentIndex();
    static uint32_t GetFrameNo(const std::vector<uint8_t> &key);

private:
    static constexpr const char *SPLIT = "_";
    static constexpr uint32_t MAX_RECORD = 1;
    static constexpr size_t HEAD_SIZE = sizeof(Event::version) + sizeof(Event::frameNum) + sizeof(Event::user) +
                                        sizeof(Event::seqId) + sizeof(Event::expiration) + sizeof(Event::status) +
                                        sizeof(uint16_t) * 3;
    template<class T>
    static bool Write(uint8_t *canvas, size_t &offset, size_t &size, T value);
    static bool Write(uint8_t *canvas, size_t &offset, size_t &size, const void *value, size_t length);

    template<class T>
    static bool Read(const uint8_t *canvas, size_t &offset, size_t &size, T &value);
    static bool Read(const uint8_t *canvas, size_t &offset, size_t &size, void *value, size_t length);
    static inline uint8_t HToLe(uint8_t value) { return value; };
    static inline uint16_t HToLe(uint16_t value) { return htole16(value); };
    static inline uint32_t HToLe(uint32_t value) { return htole32(value); };
    static inline uint64_t HToLe(uint64_t value) { return htole64(value); };
    static inline uint8_t LeToH(uint8_t value) { return value; };
    static inline uint16_t LeToH(uint16_t value) { return le16toh(value); };
    static inline uint32_t LeToH(uint32_t value) { return le32toh(value); };
    static inline uint64_t LeToH(uint64_t value) { return le64toh(value); };

    static uint32_t index_;
};
}

#endif // DISTRIBUTED_PASTEBOARD_FRAMEWORK_UTILS_H
