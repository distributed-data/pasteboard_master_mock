/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DISTRIBUTED_PASTEBOARD_FRAMEWORK_DISTRIBUTED_CLIP_H
#define DISTRIBUTED_PASTEBOARD_FRAMEWORK_DISTRIBUTED_CLIP_H
#include <mutex>
#include <shared_mutex>
#include "clip/clip_plugin.h"
#include "device/dm_adapter.h"
#include "single_kvstore.h"
#include "common/block_object.h"
namespace OHOS::MiscServices {
class DistributedClip : public ClipPlugin, protected DMAdapter::DMObserver {
public:
    using Store = DistributedKv::SingleKvStore;
    DistributedClip();
    virtual ~DistributedClip();
    int32_t SetPasteData(const GlobalEvent &event, const std::vector<uint8_t> &data) override;
    int32_t GetPasteData(const GlobalEvent &event, std::vector<uint8_t> &data) override;
    std::vector<GlobalEvent> GetTopEvents(uint32_t topN, int32_t user) override;
    void Clear(int32_t user) override;

protected:
    void Online(const std::string &device) override;
    void Offline(const std::string &device) override;

private:
    static constexpr const char *APP_ID = "pasteboard_service";
    static constexpr const char *STORE_ID = "distributed_store";
    static constexpr const char *BASE_DIR = "/data/service/el1/public/database/pasteboard_service";
    static constexpr uint32_t MAX_EVT_ADDITION = 64 * 1024;
    static constexpr uint32_t BLOCK_INTERVAL = 2 * 365 * 24 * 60 * 60; // two years no shutdown
    static constexpr uint32_t DATA_SYNC_INTERVAL = 300;
    static constexpr uint32_t EVT_SYNC_INTERVAL = 5;
    static bool RemoveDirtyData(const GlobalEvent &event, std::shared_ptr<Store> store);
    void DoSyncEvent(const std::string &evtKey, uint64_t expiration);
    void DoRetrySync(const std::vector<std::string> &devices);
    void DeInit(bool destroy = false);
    bool Initialize();


    std::shared_ptr<Store> store_;
    std::shared_mutex rwMutex_;
    std::mutex mutex_;
    std::string lastSyncKey_;
    uint64_t expiration_ = 0;
    std::shared_ptr<BlockObject<GlobalEvent>> blockObject_;
};
} // namespace OHOS::MiscServices
#endif // DISTRIBUTED_PASTEBOARD_FRAMEWORK_DISTRIBUTED_CLIP_H
